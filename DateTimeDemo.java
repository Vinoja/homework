import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

public class DateTimeDemo {
	public static void main(String[] args) {
		System.out.println("Current Date & Time");
		
		LocalDate date = LocalDate.now();
		System.out.println(date);
		
		LocalTime time = LocalTime.now();
		System.out.println(time);
		
		LocalDateTime dateTime = LocalDateTime.now();
		System.out.println(dateTime);
		
		ZoneId zoneId = ZoneId.of("America/New_York");
		LocalTime NYtime = LocalTime.now(zoneId);
		
		System.out.println("New York : " + NYtime);
	}

}
